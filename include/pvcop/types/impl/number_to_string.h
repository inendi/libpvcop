/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PVCOP_TYPES_IMPL_NUMBER_TO_STRING__H
#define PVCOP_TYPES_IMPL_NUMBER_TO_STRING__H

#ifdef LIB_ISTRCONV
#include <istrconv.h>
#endif

namespace pvcop
{

namespace types
{

namespace __impl
{

#ifndef LIB_ISTRCONV

template <typename T>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* parameters,
                            size_t /*base*/,
                            size_t /*float_precision*/,
                            char /*float_mode*/,
                            const T& value)
{
	return snprintf(str, str_len, parameters, value);
}

template <>
inline int number_to_string(char* /*str*/,
                            const size_t /*str_len*/,
                            const char* /*parameters*/,
                            size_t /*base*/,
                            size_t /*float_precision*/,
                            char /*float_mode*/,
                            const pvcop::db::uint128_t& /*value*/)
{
	assert(false && "not supported yet");
	return 0;
}

#else // LIB_ISTRCONV

template <typename T, class = typename std::enable_if<std::is_unsigned<T>::value>::type>
inline uint64_t cast(T t)
{
	return (uint64_t)t;
}

template <typename T, class = typename std::enable_if<std::is_signed<T>::value>::type>
inline int64_t cast(T t)
{
	return (int64_t)t;
}

template <typename T>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* parameters,
                            size_t base,
                            size_t float_precision,
                            char float_mode,
                            const T& value)
{
	return number_to_string(str, str_len, parameters, base, float_precision, float_mode,
	                        cast(value));
}

template <>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* parameters,
                            size_t base,
                            size_t /*float_precision*/,
                            char /*float_mode*/,
                            const int64_t& value)
{
	switch (base) {
	case 10:
		return __IML_int64_to_string(str, str_len, value);
	case 16: {
		str[0] = '0';
		str[1] = 'x';
		return __IML_int64_to_hex_string(str + 2, str_len, value) + 2;
	}
	case 8: {
		str[0] = '0';
		return __IML_int64_to_oct_string(str + (value != 0), str_len, value) + (value != 0);
	}
	default:
		return snprintf(str, str_len, parameters, value);
	}
}

template <>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* parameters,
                            size_t base,
                            size_t /*float_precision*/,
                            char /*float_mode*/,
                            const uint64_t& value)
{
	switch (base) {
	case 10:
		return __IML_uint64_to_string(str, str_len, value);
	case 16: {
		str[0] = '0';
		str[1] = 'x';
		return __IML_uint64_to_hex_string(str + 2, str_len, value) + 2;
	}
	case 8: {
		str[0] = '0';
		return __IML_uint64_to_oct_string(str + (value != 0), str_len, value) + (value != 0);
	}
	default:
		return snprintf(str, str_len, parameters, value);
	}
}

template <>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* /*parameters*/,
                            size_t /*base*/,
                            size_t float_precision,
                            char float_mode,
                            const double& value)
{
	switch (float_mode) {
	case 'e':
		return __IML_double_to_string_e(str, str_len, float_precision, value);
	case 'f':
		return __IML_double_to_string_f(str, str_len, float_precision, value);
	case 'g':
		return __IML_double_to_string(str, str_len, float_precision, value);
	default:
		assert(false);
		return {};
	}
}

template <>
inline int number_to_string(char* str,
                            const size_t str_len,
                            const char* /*parameters*/,
                            size_t /*base*/,
                            size_t float_precision,
                            char float_mode,
                            const float& value)
{
	switch (float_mode) {
	case 'e':
		return __IML_float_to_string_e(str, str_len, float_precision, value);
	case 'f':
		return __IML_float_to_string_f(str, str_len, float_precision, value);
	case 'g':
		return __IML_float_to_string(str, str_len, float_precision, value);
	default:
		assert(false);
		return {};
	}
}

#endif // LIB_ISTRCONV

} // namespace pvcop::types::__impl

} // namespace pvcop::types

} // namespace pvcop

#endif // PVCOP_TYPES_IMPL_NUMBER_TO_STRING__H
