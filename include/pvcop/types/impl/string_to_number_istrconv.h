/* * MIT License
 *
 * © ESI Group, 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PVCOP_TYPES_IMPL_STRING_TO_NUMBER_H
#define PVCOP_TYPES_IMPL_STRING_TO_NUMBER_H

#include <pvcop/db/types.h>

#include <cstdlib>
#include <string.h>
#include <cmath>
#include <istrconv.h>

namespace pvcop
{

namespace types
{

namespace __impl
{

template <typename T, typename F1, typename F2, typename F3>
static inline T istrconv_to_string(
    const F1& f1, const F2& f2, const F3& f3, const char* str, char** tmp, size_t base)
{
	// explicitely check for base prefix as istrconv doesn't do it
	*tmp = const_cast<char*>(str);
	size_t len = strlen(str);
	switch (base) {
	case 10: {
		return f1(str, tmp);
	}
	case 16:
		if (len >= 2 and str[0] == '0' and str[1] == 'x') {
			return f2(str + 2, tmp);
		}
		return 0;
	case 8:
		if (len >= 1 and str[0] == '0') {
			return f3(str + 1, tmp);
		}
		return 0;
	}

	assert(false);
	return 0;
}

/**
 * using Intel libistrconv function to do the parsing.
 */
inline int64_t istrconv_string_to_int(const char* str, char** tmp, size_t base)
{
	return istrconv_to_string<int64_t>(__IML_string_to_int, __IML_hex_string_to_int,
	                                   __IML_oct_string_to_int, str, tmp, base);
}
inline uint64_t istrconv_string_to_uint(const char* str, char** tmp, size_t base)
{
	return istrconv_to_string<uint64_t>(__IML_string_to_uint64, __IML_hex_string_to_uint64,
	                                    __IML_oct_string_to_uint64, str, tmp, base);
}

inline int64_t istrconv_string_to_int64(const char* str, char** tmp, size_t base)
{
	return istrconv_to_string<int64_t>(__IML_string_to_int64, __IML_hex_string_to_int64,
	                                   __IML_oct_string_to_int64, str, tmp, base);
}

inline uint64_t istrconv_string_to_uint64(const char* str, char** tmp, size_t base)
{
	return istrconv_to_string<uint64_t>(__IML_string_to_uint64, __IML_hex_string_to_uint64,
	                                    __IML_oct_string_to_uint64, str, tmp, base);
}

template <typename T, typename V>
inline bool in_range(V value)
{
	return value >= std::numeric_limits<T>::lowest() && value <= std::numeric_limits<T>::max();
}

template <typename T>
inline T string_to_number(const char*, const size_t, bool& res, bool* /*pass_autodetect*/);

template <>
inline string_index_t
string_to_number<string_index_t>(const char*, const size_t, bool& res, bool* /*pass_autodetect*/)
{
	assert(false && "should not be used");
	res = false;
	return 0;
}

template <>
inline bool
string_to_number<bool>(const char* str, const size_t, bool& res, bool* /*pass_autodetect*/)
{
	res = true;

	if ((strncmp(str, "0", 1) == 0 or strncasecmp(str, "false", 5) == 0)) {
		return false;
	}
	if ((strncmp(str, "1", 1) == 0 or strncasecmp(str, "true", 4) == 0)) {
		return true;
	}

	res = false;
	return false;
}

template <>
inline float
string_to_number<float>(const char* str, const size_t, bool& res, bool* pass_autodetect)
{
	errno = 0;
	char* tmp;

	float value = __IML_string_to_float(str, &tmp);

	res = not(tmp == str || *tmp != '\0' || errno != 0);

	if (pass_autodetect) {
		*pass_autodetect = res;
	}

	return value;
}

template <>
inline double
string_to_number<double>(const char* str, const size_t, bool& res, bool* pass_autodetect)
{
	errno = 0;
	char* tmp;

	double value = __IML_string_to_double(str, &tmp);

	res = not(tmp == str || *tmp != '\0' || errno != 0);

	if (pass_autodetect) {
		*pass_autodetect = res;
	}

	return value;
}

template <>
inline int8_t
string_to_number<int8_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	int64_t value = istrconv_string_to_int(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<int8_t>(value);

	return value;
}

template <>
inline uint8_t
string_to_number<uint8_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	uint64_t value = istrconv_string_to_uint(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<uint8_t>(value);

	return value;
}

template <>
inline int16_t
string_to_number<int16_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	int64_t value = istrconv_string_to_int(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<int16_t>(value);

	return value;
}

template <>
inline uint16_t
string_to_number<uint16_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	uint64_t value = istrconv_string_to_uint(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<uint16_t>(value);

	return value;
}

template <>
inline int32_t
string_to_number<int32_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	int64_t value = istrconv_string_to_int(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<int32_t>(value);

	return value;
}

template <>
inline uint32_t
string_to_number<uint32_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	uint64_t value = istrconv_string_to_uint(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0) && in_range<uint32_t>(value);

	return value;
}

template <>
inline int64_t
string_to_number<int64_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	int64_t value = istrconv_string_to_int64(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0);

	return value;
}

template <>
inline uint64_t
string_to_number<uint64_t>(const char* str, const size_t base, bool& res, bool* /*pass_autodetect*/)
{
	errno = 0;
	char* tmp;

	/**
	 * From strtoul man page :
	 * "Negative values are considered valid input and are silently converted
	 *  to the equivalent unsigned long int value."
	 *  So we have to check for negative values by ourselves
	 **/
	if (str[0] == '\0' or str[0] == '-') {
		res = false;
		return 0;
	}
	uint64_t value = istrconv_string_to_uint64(str, &tmp, base);

	res = not(tmp == str || *tmp != '\0' || errno != 0);

	return value;
}

template <>
inline pvcop::db::uint128_t string_to_number<pvcop::db::uint128_t>(const char* /*str*/,
                                                                   const size_t /*base*/,
                                                                   bool& /*res*/,
                                                                   bool* /*pass_autodetect*/)
{
	assert(false && "not supported yet");
	return {};
}

} // namespace pvcop::types::__impl

} // namespace pvcop::types

} // namespace pvcop

#endif // PVCOP_TYPES_IMPL_STRING_TO_NUMBER_H
